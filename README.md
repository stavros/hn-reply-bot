HN reply bot
============

I needed a quick way to receive notifications for replies to my comments on
Hacker News, so I wrote this bot. It will send you a Telegram message every
time someone replies to one of your comments.

Due to a quirk of my comment handling code more than anything else, it will
also send you notifications for comment replies to your stories.

To use it, just go to:

https://t.me/hn_replies_bot

and tell the bot your HN username. That's it!
